-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, vector
    = minetest, nodecore, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

minetest.register_privilege(modname, "Can summon Dais Ex Machina")

nodecore.register_craft({
		label = "summon dais ex machina",
		action = "pummel",
		duration = 4,
		wield = {name = "nc_tree:eggcorn", count = false},
		check = function(_, data)
			return data.crafter
			and minetest.get_player_privs(data.crafter)[modname]
			and (not exmachina.location_get(data.crafter))
		end,
		nodes = exmachina.craftnodes_summon,
		after = function(pos, data)
			local pname = data.pname or data.crafter:get_player_name()
			return exmachina.summon_start(pos, pname)
		end
	})

minetest.register_abm({
		label = "Dais Ex Machina Summoning Completion",
		interval = 1,
		chance = 1,
		nodenames = {modname .. ":nexus"},
		action = function(pos)
			if exmachina.summon_occupied(pos) then
				return exmachina.areapuffs(pos, 25, 1)
			end
			local pname = minetest.get_meta(pos):get_string(modname)
			return exmachina.summon_complete(pos, pname)
		end
	})

nodecore.register_craft({
		label = "banish dais ex machina",
		action = "pummel",
		duration = 4,
		wield = {name = "nc_tree:eggcorn", count = false},
		check = function(pos, data)
			if not minetest.get_player_privs(data.crafter)[modname]
			then return end
			local ppos = exmachina.location_get(data.crafter)
			return ppos and vector.equals(pos, ppos)
			and minetest.get_meta(pos):get_string(modname)
			== data.crafter:get_player_name()
		end,
		nodes = exmachina.craftnodes_banish,
		after = function(pos, data)
			local pname = data.pname or data.crafter:get_player_name()
			exmachina.banish(pos, pname, true)
		end
	})
