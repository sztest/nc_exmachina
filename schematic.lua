-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, table
    = ipairs, minetest, nodecore, table
local table_concat
    = table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

local function expandify(q, e, m, c)
	local t = {e}
	for _ = 1, q do t[#t + 1] = m end
	t[#t + 1] = c or m
	for _ = 1, q do t[#t + 1] = m end
	t[#t + 1] = e
	return t
end

-- _ = air
-- c = cobble
-- e = eggcorn
-- f = bottom frame
-- u = upper frame
-- x = core

local fffff = table_concat(expandify(exmachina.extent, "f", "f"))
local fcfcf = table_concat(expandify(exmachina.extent, "f", "c", "f"))
local ffxff = table_concat(expandify(exmachina.extent, "f", "f", "x"))
local uuuuu = table_concat(expandify(exmachina.extent, "u", "u"))
local u___u = table_concat(expandify(exmachina.extent, "u", "_"))
local e___e = table_concat(expandify(exmachina.extent, "e", "_"))
local empty = table_concat(expandify(exmachina.extent, "_", "_"))

local bot = expandify(exmachina.extent, fffff, fcfcf, ffxff)
local sec = expandify(exmachina.extent, e___e, empty)
local mid = expandify(exmachina.extent, u___u, empty)
local top = expandify(exmachina.extent, uuuuu, u___u)

local full = {bot, sec}
for _ = 2, exmachina.height do full[#full + 1] = mid end
full[#full + 1] = top

exmachina.schematic_final = nodecore.ezschematic(
	{
		_ = {name = "air", prob = 255},
		c = {name = "nc_terrain:cobble", prob = 255},
		x = {name = modname .. ":core", prob = 255},
		f = {name = modname .. ":frame", prob = 255},
		e = {name = modname .. ":frame", prob = 255},
		u = {name = modname .. ":frame", prob = 255},
	},
	full
)

exmachina.schematic_trans = nodecore.ezschematic(
	{
		_ = {name = modname .. ":field", prob = 255},
		c = {name = modname .. ":field", prob = 255},
		x = {name = modname .. ":nexus", prob = 255},
		f = {name = modname .. ":field", prob = 255},
		e = {name = modname .. ":field", prob = 255},
		u = {name = modname .. ":field", prob = 255},
	},
	full
)

local function schemremove(prob)
	return nodecore.ezschematic(
		{
			_ = {name = "air", prob = prob},
			c = {name = "nc_terrain:cobble", prob = prob},
			x = {name = "nc_terrain:dirt_loose", prob = 255},
			f = {name = "nc_terrain:cobble", prob = 255},
			e = {name = "air", prob = 255},
			u = {name = "air", prob = 255},
		},
		full
	)
end

exmachina.schematic_banish = schemremove(255)
exmachina.schematic_degrade = schemremove(0)

local function mkcraftnodes(key)
	local data = {}
	for y, ys in ipairs(full) do
		for z, zs in ipairs(ys) do
			for x = 1, zs:len() do
				local match = key[zs:sub(x, x)]
				if match then
					data[#data + 1] = {
						x = x - exmachina.extent - 2,
						y = y - 1,
						z = z - exmachina.extent - 2,
						match = match
					}
				end
			end
		end
	end
	return data
end

local aireq = {air_equivalent = true}

exmachina.craftnodes_summon = mkcraftnodes({
		_ = aireq,
		c = "nc_terrain:cobble",
		x = "nc_tree:eggcorn_planted",
		f = "nc_terrain:cobble",
		e = "nc_tree:eggcorn",
		u = aireq,
	})

exmachina.craftnodes_banish = mkcraftnodes({
		_ = aireq,
		c = "nc_terrain:cobble",
		x = modname .. ":core",
		f = modname .. ":frame",
		e = modname .. ":frame",
		u = modname .. ":frame",
	})

exmachina.craftnodes_operating = {}
do
	local keepers = {
		[modname .. ":core"] = true,
		[modname .. ":frame"] = true,
	}
	local mismatch = {groups = {[modname] = false}, count = false}
	for i, v in ipairs(exmachina.craftnodes_banish) do
		exmachina.craftnodes_operating[i] = {
			x = v.x,
			y = v.y,
			z = v.z,
			match = keepers[v.match] and v.match or mismatch
		}
	end
end
