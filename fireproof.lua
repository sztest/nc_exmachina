-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, string
    = minetest, nodecore, string
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

if minetest.settings:get_bool(modname .. "_allowfire") then return end

local old_ignite = nodecore.fire_ignite
function nodecore.fire_ignite(pos, ...)
	local owner = exmachina.owner_search(pos)
	if owner then
		minetest.log("action", string_format("ignition blocked at %s by"
				.. " dais of %s", minetest.pos_to_string(pos), owner))
		return
	end
	return old_ignite(pos, ...)
end
