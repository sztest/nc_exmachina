-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, os, table
    = math, minetest, nodecore, os, table
local math_ceil, os_date, table_concat
    = math.ceil, os.date, table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

local saveddesc = "Last Save: @1 Z"
nodecore.translate_inform(saveddesc)
local nextdesc = "Next Save: @1"
nodecore.translate_inform(nextdesc)
local brokendesc = nodecore.translate("BROKEN")

local coredesc = "Dais Ex Machina Core"
minetest.register_node(modname .. ":core", {
		description = coredesc,
		groups = {[modname] = 1},
		tiles = {
			exmachina.txr_core,
			exmachina.txr_core,
			exmachina.txr_frame
		},
		on_node_touchtip = function(pos)
			local meta = minetest.get_meta(pos)
			local owner = meta:get_string(modname)
			local lines = {coredesc}
			if owner and owner ~= "" then
				lines[#lines + 1] = nodecore.notranslate(owner)
			end
			local saved = meta:get_string("saved")
			saved = saved and saved ~= "" and minetest.deserialize(saved, true) or nil
			local cdown = nodecore.dnt_get and nodecore.dnt_get(pos, modname)
			if saved or cdown then lines[#lines + 1] = " " end
			if saved then
				lines[#lines + 1] = nodecore.translate(saveddesc,
					os_date("!%Y-%m-%d %H:%M:%S", saved))
			end
			if cdown then
				lines[#lines + 1] = nodecore.translate(nextdesc,
					cdown <= 0 and 0 or math_ceil(cdown))
			end
			if meta:get_string("broken") ~= "" then
				lines[#lines + 1] = " "
				lines[#lines + 1] = brokendesc
			end
			return table_concat(lines, "\n")
		end,
		sounds = nodecore.sounds("nc_terrain_stony"),
		mapcolor = {r = 72, g = 72, b = 72},
	})

minetest.register_node(modname .. ":frame", {
		description = "Dais Ex Machina Frame",
		groups = {[modname] = 1},
		tiles = {exmachina.txr_frame},
		sounds = nodecore.sounds("nc_terrain_stony"),
		mapcolor = {r = 72, g = 72, b = 72},
	})

minetest.register_node(modname .. ":field", {
		drawtype = "allfaces",
		groups = {[modname] = 1},
		tiles = {{
				name = exmachina.txr_sparkle,
				backface_culling = false
		}},
		paramtype = "light",
		light_source = 3,
		sunlight_propagates = true,
		walkable = false,
		pointable = false,
		climbable = true,
		mapcolor = {r = 255, g = 255, b = 255, a = 64},
	})

minetest.register_node(modname .. ":nexus", {
		drawtype = "allfaces",
		groups = {[modname] = 1},
		tiles = {{
				name = exmachina.txr_sparkle,
				backface_culling = false
		}},
		paramtype = "light",
		sunlight_propagates = true,
		walkable = false,
		pointable = false,
		climbable = true,
		mapcolor = {r = 72, g = 72, b = 72},
	})
