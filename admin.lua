-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, string, vector
    = minetest, nodecore, string, vector
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

minetest.register_privilege(modname .. "_admin", {
		description = "Can use Dais Ex Machina admin commands",
		give_to_singleplayer = false,
		give_to_admin = true
	})

minetest.register_chatcommand("exmachina", {
		description = "Get Dais Ex Machina location",
		params = "<playername>",
		privs = {[modname .. "_admin"] = true},
		func = function(_, param)
			if not param or param == "" then
				return false, "Missing player name"
			end
			local pos = exmachina.location_get(param)
			if not pos then
				return false, "Platform not summoned"
			end
			return true, string_format("Platform for %q located at %s",
				param, minetest.pos_to_string(pos))
		end
	})

minetest.register_chatcommand("exmachina_banish", {
		description = "Force unsummon Dais Ex Machina",
		params = "<playername>",
		privs = {[modname .. "_admin"] = true},
		func = function(_, param)
			if not param or param == "" then
				return false, "Missing player name"
			end
			local pos = exmachina.location_get(param)
			if not pos then
				return false, "Platform not summoned"
			end
			if not exmachina.arealoaded(pos) then
				return false, "Platform area not fully loaded"
			end
			return exmachina.banish(pos, param)
		end
	})

minetest.register_chatcommand("exmachina_degrade", {
		description = "Destroy Dais Ex Machina, convert back to materials",
		params = "<playername>",
		privs = {[modname .. "_admin"] = true},
		func = function(_, param)
			if not param or param == "" then
				return false, "Missing player name"
			end
			local pos = exmachina.location_get(param)
			if not pos then
				return false, "Platform not summoned"
			end
			if not exmachina.arealoaded(pos) then
				return false, "Platform area not fully loaded"
			end
			return exmachina.degrade(pos, param)
		end
	})

minetest.register_chatcommand("exmachina_summon", {
		description = "Force summon Dais Ex Machina",
		params = "<playername>",
		privs = {[modname .. "_admin"] = true},
		func = function(pname, param)
			local player = minetest.get_player_by_name(pname)
			if not player then
				return false, "Cannot summon from console"
			end
			if not param or param == "" then
				return false, "Missing player name"
			end
			if exmachina.location_get(param) then
				return false, "Platform already summoned"
			end
			local pos = vector.round(player:get_pos())
			if not exmachina.arealoaded(pos) then
				return false, "Platform area not fully loaded"
			end
			local minpos, maxpos = exmachina.getbounds(pos)
			for x = minpos.x, maxpos.x do
				for y = minpos.y, maxpos.y do
					for z = minpos.z, maxpos.z do
						if not (minetest.registered_nodes[minetest.get_node({x = x, y = y, z = z}).name] or {}).air_equivalent then
							return false, "Area not fully cleared"
						end
					end
				end
			end
			return exmachina.summon_start(pos, param)
		end
	})
