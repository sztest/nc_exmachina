-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, tonumber
    = minetest, nodecore, tonumber
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

local squelch = {}

minetest.register_on_protection_violation(function(pos, name)
		if not pos.alarm then return end

		local gain = tonumber(minetest.settings:get(modname .. "_alarm")) or 1
		if gain <= 0 then return end

		local owner = exmachina.owner_search(pos)
		if not owner then return end
		local cpos = exmachina.location_get(owner)
		if not cpos then return end

		local hash = minetest.hash_node_position(cpos)
		local t = squelch[hash]
		local now = minetest.get_us_time()
		if t and t > now - 1000000 then return end
		squelch[hash] = now

		local minpos, maxpos = exmachina.getbounds(cpos)

		for x = minpos.x, maxpos.x, maxpos.x - minpos.x do
			for y = minpos.y, maxpos.y, maxpos.y - minpos.y do
				for z = minpos.z, maxpos.z, maxpos.z - minpos.z do
					nodecore.sound_play(modname .. "_denied", {
							gain = gain,
							pos = {x = x, y = y, z = z},
							pitchvary = 0.01
						})
				end
			end
		end

		local player = minetest.get_player_by_name(name)
		if not player then return end
		nodecore.hud_set(player, {
				label = modname .. "_denied",
				hud_elem_type = "text",
				position = {x = 0.5, y = 0.55},
				text = "ACCESS DENIED",
				number = 0xFF0000,
				alignment = {x = 0, y = 1},
				offset = {x = 0, y = 0},
				ttl = 2
			})
	end)
