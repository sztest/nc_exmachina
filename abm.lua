-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, os, pairs, string, tonumber, type, vector
    = minetest, nodecore, os, pairs, string, tonumber, type, vector
local os_time, string_format
    = os.time, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local exmachina = nodecore[modname]

local interval = tonumber(minetest.settings:get(modname .. "_interval")) or 60

minetest.mkdir(exmachina.path)

local function setbroken(pos, pname, coremeta, reason)
	local value = reason and "1" or ""
	if coremeta:get_string("broken") == value then return end
	nodecore.log("warning", string_format("%s: dais for %s %s at %s",
			modname, pname, reason and ("broken (" .. reason .. ")") or "repaired",
			minetest.pos_to_string(pos)))
	coremeta:set_string("broken", value)
end

nodecore.register_dnt({
		name = modname,
		time = interval,
		nodenames = {modname .. ":core"},
		loop = true,
		action = function(pos)
			local pname = minetest.get_meta(pos):get_string(modname)
			if (pname or "") == "" then return end

			local coremeta = minetest.get_meta(pos)
			local regpos = exmachina.location_get(pname)
			if not (regpos and vector.equals(pos, regpos)) then
				return setbroken(pos, pname, coremeta, "registered at "
					.. minetest.pos_to_string(regpos))
			end

			if not exmachina.arealoaded(pos) then return end

			if not exmachina.validate(pos) then
				return setbroken(pos, pname, coremeta, "validation failed")
			end
			setbroken(pos, pname, coremeta, nil)

			if not minetest.player_exists(pname) then
				nodecore.log("action", "dais for missing player " .. pname .. " at "
					.. minetest.pos_to_string(pos) .. " automatically degraded")
				return exmachina.degrade(pos, pname)
			end

			local pathbase = exmachina.pathbase(pname)
			local minpos, maxpos = exmachina.getbounds(pos)

			minetest.create_schematic(minpos, maxpos, nil, pathbase .. ".mts")

			coremeta:set_string("saved", minetest.serialize(os_time()))

			local allmeta = {}
			for _, p in pairs(minetest.find_nodes_with_meta(minpos, maxpos)) do
				local meta = minetest.get_meta(p):to_table()
				for _, v1 in pairs(meta.inventory or {}) do
					for k2, v2 in pairs(v1) do
						if type(v2) == "userdata" then
							v1[k2] = v2:to_string()
						end
					end
				end
				allmeta[vector.subtract(p, pos)] = meta
			end
			minetest.safe_file_write(
				pathbase .. ".meta",
				minetest.serialize(allmeta)
			)

			nodecore.log("action", string_format("%s: saved dais for %s at %s",
					modname, pname, minetest.pos_to_string(pos)))

			minetest.add_particlespawner({
					amount = 10,
					time = 1,
					minpos = vector.add(pos, {x = -0.5, y = 0.55, z = -0.5}),
					maxpos = vector.add(pos, {x = 0.5, y = 0.75, z = 0.5}),
					minacc = {x = -0.5, y = 1, z = -0.5},
					maxacc = {x = 0.5, y = 2, z = 0.5},
					minexptime = 2,
					maxexptime = 3,
					minsize = 1,
					maxsize = 2,
					collisiondetection = true,
					texture = exmachina.txr_puff
				})
		end
	})

minetest.register_abm({
		label = "Ex Machina Save",
		interval = interval,
		chance = 1,
		nodenames = {modname .. ":core"},
		action = function(pos) nodecore.dnt_set(pos, modname) end
	})
nodecore.register_lbm({
		name = modname .. ":corednt",
		run_at_every_load = true,
		nodenames = {modname .. ":core"},
		action = function(pos) nodecore.dnt_set(pos, modname) end
	})
