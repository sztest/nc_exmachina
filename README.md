This mod was created primarily for the use-case of the official unofficial NodeCore multiplayer server.  Due to ongoing development on map generation and ore distribution, we sometimes reset the world.  However, players don't always want to start from scratch every time.  This mod provides a not-totally-gameplay-breaking method for players to preserve some of their property across resets.

**For Players:**

* One platform exists for each player, and players can only summon their own platform.
* A platform can only exist in one place in the world at a time, and cannot be re-summoned once already placed.
* A platform can only be moved by summoning or banishing, and cannot be dug. †
* Anything inside the platform, other than the core and frame itself, can be modified.  This includes the cobble floor sections it ships with.
* Every few seconds, the platform's core emits a puff of particles to indicate its contents synced off-world successfully.
* Everything in node-space is saved, including placed nodes, shelves/containers and their contents, all settled item stacks, node inventories and metadata.  Moving entities, players, and player inventories are not stored.
* After a world reset, summoning the platform in the new world will restore its saved contents from the previous world.
* The recipe for summoning the platform is a square cobble platform of the same configured width as the dais (default 9), with a hole in the middle filled by an Eggcorn sprout, and one Eggcorn on each of the 4 platform corners.  Pummel the sprout with another Eggcorn.
* Everything else in the space where the platform is to be summoned must be air (anything else, including water, must be excavated).
* Fire may not spread into or within a dais (by default).
* Access restrictions on the platform's contents are configurable.  By default, solid lode cubes, lode crates and their contents, and totes and packed contents are protected, and only the platform's owner can access them.
*  It is possible for players to place things into someone else's dais (via dropping/throwing) that they then can't pick back up.  This is a known issue and not preventable.
* A platform can be banished by the player who owns it by pummeling the core with an eggcorn, but only if it's returned to *exact* "original" condition (empty except for the cobble floor).

The default platform size (configurable) is **9x9 at the base, 5 high**.

**For Server Operators:**

* When resetting the map, remove `map.sqlite`, `players.sqlite`, and `mod_storage` (or corresponding artifacts for your back-end).
* Make sure the `nc_exmachina` dir is *preserved*.
* Keep `env_meta.txt`, and add a large number of seconds (eg 32 million for each year the map existed) to gametime as a safety margin.  This ensures things inside the dais do not experience reverse time flow. 
* If you want a new map seed, you can remove or reset `map_meta.txt`, otherwise you will get a fresh copy of the same terrain.
* The platform size is configurable, but platforms can only be safely restored in worlds where the size configuration matches.
* If you are using any tool, such as worldedit, to preserve sections of the map:
  * The best option is to ensure no dais exists within any preserved area. Use `/exmachina_banish` to banish them.
  * If you preseve any dais in-world, you will need to manually edit the `mod_storage/nc_exmachina` file and ensure its core position is registered in the new world, but all daises not transferred are removed.

*†  Moving or otherwise modifying the platform via worldedit or similar mods may be possible, but voids its warranty.*
